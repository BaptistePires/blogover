<?php

include "header.php"

?>
	<div id="body">
		<h2>Classes</h2>
		<div class="content">
			<div>
				<div class="section">
					<h3>Beginners</h3>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in neque condimentum, dignissim libero sit amet, blandit felis.
					</p>
					<span>MWF : 8AM to 9AM</span>
					<span>TTHS : 10AM-11AM</span>
				</div>
				<div class="section">
					<h3>Intermediate</h3>
					<p>
						Cras dignissim est sed lorem suscipit, ut ultrices dolor tempus. Cras venenatis gravida scelerisque. Aenean sit amet massa dui
					</p>
					<span>MWF : 10AM-11AM</span>
					<span>TTHS : 1PM-2PM</span>
				</div>
				<div class="section">
					<h3>Advanced</h3>
					<p>
						Quisque non quam lorem. Nulla eu placerat leo. Suspendisse eros risus, viverra sed fermentum vitae, gravida ac tellus.
					</p>
					<span>MWF : 1PM-2PM</span>
					<span>TTHS : 8AM to 9AM</span>
				</div>
			</div>
			<img src="images/lady-in-yoga.jpg" alt="lady doing yoga" class="figure">
		</div>
	</div>
	<div id="footer">
		<div>
			<span>123 St. City Location, Country | 987654321</span>
			<p>
				&copy; 2023 by Belle &amp; Carrie Rehabilitation Yoga. All rights reserved.
			</p>
		</div>
		<div id="connect">
			<a href="https://freewebsitetemplates.com/go/facebook/" id="facebook" target="_blank">Facebook</a>
			<a href="https://freewebsitetemplates.com/go/twitter/" id="twitter" target="_blank">Twitter</a>
			<a href="https://freewebsitetemplates.com/go/googleplus/" id="googleplus" target="_blank">Google&#43;</a>
			<a href="https://freewebsitetemplates.com/go/pinterest/" id="pinterest" target="_blank">Pinterest</a>
		</div>
	</div>
<?php

include "footer.php"

?>